namespace my.shop;
using { User, managed } from '@sap/cds/common';

entity Categories {
  key ID	: Integer;
  catName	: String;
  prod  	: Association [0..*] to Product on prod.category = $self;
}

entity Product {
  key ID		: Integer;
  prodName		: String;
  price			: Integer;
  stock			: Integer;
  description	: String;
  category		: Association[1] to Categories {ID};
  comment		: Association [0..*] to Comments on comment.prodId = $self;
}

entity Comments : managed {
  key ID	: UUID;
  comment	: String;
  prodId	: Association[1] to Product {ID};
}