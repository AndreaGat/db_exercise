using { my.shop, sap.common } from '../db/data-model';

service CatalogService @(requires: ['ShowService','system-user'] ) {
  entity Categories as projection on shop.Categories;
  entity Product as projection on shop.Product;
  entity Comments as projection on shop.Comments;
}